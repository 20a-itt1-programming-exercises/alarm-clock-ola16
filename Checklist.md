**Alarm clock with Youtube integration OLA 16**

**check list first itteration**

- [x] Make flowchart
- [x] writing and testing the raw code
- [x] Basic features set time for alarm execution 
- [x] Make alarm for EU time format [Hour:Minute:Second]
- [x] User input prompt for alarm.
- [x] Check for right int value else ERROR.
- [x] Display amount of time untill alarm execution "00:00:00"
- [x] Test code

**extra features. (if enough time)**

- [x] Youtube Video playback from browser integration
- [x] Automatic creation of youtube-video-list.txt file (userfriendly,)
- [x] Prompt the user first time the list is created and the loction to input link.
- [x] Randomly play a link from the youtube-video-list.txt list created
- [x] Test final product.



